#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#define BUZZER_PIN 7
#define LED_PIN 11
#define NAVIGATION_PIN 12
#define CONFIRM 13
#define LUMINOSITY_PIN A0
#define DHT_PIN 8
#define POTENTIOMETER A2

#define REVERSE_LDR true

// These variables define how many points are taken into consideration
// when reading and showing the data.
#define POINTS_TEMPERATURE 10
#define POINTS_HUMIDITY 10
#define POINTS_LUMINOSITY 10

#define BUTTON_CLICK_INTERVAL_MS 200
//#define EEPROM_ALERT_SAVING_INTERVAL_MS 60000 // save every 60 seconds
#define EEPROM_ALERT_SAVING_INTERVAL_MS 60000 // save every 15 seconds
//#define EEPROM_ALERT_SAVING_INTERVAL_MS 5000 // save every 5 seconds

#define DEBUG_MODE true

#endif
