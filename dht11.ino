#include "DHT.h"

#define DHT_TYPE DHT11

// DHT11 Sensor Reader
DHT dht(DHT_PIN, DHT_TYPE);

void initDHT() {
  dht.begin();
}

// returns temperature in celsius
float readTemperature()
{
  // Read temperature as Celsius
  float temperature = dht.readTemperature();

  if (isnan(temperature)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return 0;
  }

  return temperature;
}

// returns humidity in a 0-100 scale
int readHumidity()
{
  float humidity = dht.readHumidity();

  //   // Check the result of the reading.
  //   // If there's no error, print the humidity value.
  //   // If there's an error, print the appropriate error message.
  // Check if any reads failed and exit early (to try again).
  if (isnan(humidity)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return 0;
  }

  return humidity;

}
