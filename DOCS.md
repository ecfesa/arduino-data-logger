# Documentação Técnica

## Componentes Principais

- 1 Placa Arduino Uno R3 (MCU Atmega 328P);
- 1 Fotoresistor (LDR)
- 1 Resistor 10 kOhm
- 1 Sensor DHT-11
- 1 LCD I2C 16x2 
- 1 Bateria de 9V
- 1 RTC (módulo Real Time Clock)
- 1 LED

## Pinagem e Configurações

### Pinagem de Entrada/Saída:

- BUZZER: Pino de saída para o dispositivo emissor de som.
- LED: Pino de saída para o LED indicador.
- NAVIGATION: Pino de entrada para navegação entre telas.
- LUMINOSITY: Pino de entrada para o sensor de luminosidade.
- DHT: Pino de entrada para o sensor DHT11 (temperatura e umidade).
- POTENTIOMETER: Pino de entrada para o potenciômetro de ajuste.

### Conexões I2C:

Para a comunicação com o display de LCD, utiliza-se o protocolo I2C com o endereço 0x27.

## Funcionalidades

### Monitoramento Ambiental

O sistema realiza a leitura da temperatura, umidade e luminosidade do ambiente de forma precisa e confiável. Para medição dos dados de temperatura, em graus Celsius, e umidade relativa do ar, em porcentagem (%), utilizou-se o sensor DHT11, um dispositivo relativamente simples de usar e que possui um protocolo de comunicação digital, o que o torna compatível com diversos tipos de placas e módulos.

Para medição da luminosidade, foi utiliza uma fotocélula LDR comum.

Tanto as medições de umidade e luminosidade, passam por uma conversão com o objetivo de exibir os valores em escalas adequadas, variando de 0 a 100, para uma análise clara do ambiente.

### Relógio Digital

Além de monitorar o ambiente, o sistema também funciona como um relógio digital preciso, utilizando o módulo RTC DS3231 para manter o horário correto. Este módulo é um circuito integrado que possui um relógio interno preciso, capaz de manter a contagem de tempo mesmo quando o sistema está desligado ou sem energia externa.

### Alarme de Condições Anormais

Para garantir a segurança e alertar sobre condições fora dos limites pré-estabelecidos, o sistema ativa o LED indicador e o buzzer em situações onde a temperatura, umidade ou luminosidade ultrapassam os valores aceitáveis. Esses alertas são armazenados na EEPROM para registro e análise posterior.

Os ranges dos gatilhos podem ser descritos da seguinte forma:

* Faixa de Temperatura: O sistema considera como condição normal uma temperatura ambiente entre 15ºC e 25ºC. Valores abaixo de 15ºC ou acima de 25ºC acionam o alerta de temperatura fora dos limites.

* Faixa de Luminosidade: Para a luminosidade, o sistema considera como condição normal uma porcentagem entre 0% e 30%. Valores acima de 30% acionam o alerta de luminosidade excessiva.

* Faixa de Umidade: Em relação à umidade, o sistema considera como condição normal uma faixa entre 30% e 50%. Valores abaixo de 30% ou acima de 50% acionam o alerta de umidade fora dos limites.
