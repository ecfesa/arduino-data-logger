# Manual de Operação

Este é um projeto de Data Logger desenvolvido com base na plataforma Arduino UNO R3. O dispositivo é capaz de medir temperatura, umidade relativa do ar e luminosidade, além de registrar e alertar quando os valores ficam fora dos níveis de referência.

## Instalação e Montagem:

1. Conecte os componentes conforme o diagrama elétrico fornecido.
2. Certifique-se de que todas as conexões estão seguras e corretas.
3. Insira a bateria de 9V no suporte para alimentar o Arduino.

## Interface do LCD:

- O display LCD mostrará as informações de temperatura, umidade e luminosidade.
- Caso os valores estejam dentro dos níveis aceitáveis, serão exibidos normalmente.
- Se algum parâmetro estiver fora da faixa aceitável, o valor será acompanhado por um alerta visual e sonoro.
- Você poderá alterar entre os modos de visualização do display LCD ao pressionar o botão presente no protoboard.

## Interpretação dos Alertas:

O alerta visual e sonoro será acionado nas seguintes situações:
- Caso a temperatura esteja abaixo de 15°C ou acima de 25°C.
- Se a luminosidade estiver abaixo de 0% ou acima de 30%.
- Se a umidade estiver abaixo de 30% ou acima de 50%.

## Salvamento de dados:

O dispositivo realiza o backup das leituras na memória embutida "EEPROM", incluindo a data e a hora da ocorrência e os valores médios ponderados. Os backups são realizados quando os parâmetros estão fora dos níveis de referência.

## Desligamento do Dispositivo:

Para desligar o Data Logger, pressione e segure o botão de alimentação por alguns segundos até que o display LCD se apague.

## Manutenção:

- Verifique regularmente a integridade dos componentes e a precisão das medições.
- Substitua a bateria quando necessário para garantir o funcionamento contínuo do dispositivo.

## Segurança:

- Este dispositivo utiliza eletricidade e componentes sensíveis. Mantenha-o longe de fontes de calor, umidade e choque.
- Evite manipular os componentes com as mãos molhadas ou em ambientes perigosos.

