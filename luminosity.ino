#include "globals.h"

int mappedValue; // Variable to store the mapped value

LDRCalibrationData ldrCalibration;

void setupLuminosity() {
  ldrCalibration.minimumValue = -1;
  ldrCalibration.maximumValue = -1;

  EEPROM.get(ADDRESS_LDR_CALIBRATION_DATA, ldrCalibration);
  Serial.print("[LDR] Loaded Initial Minimum Value: ");
  Serial.println(ldrCalibration.minimumValue);
  Serial.print("[LDR] Loaded Initial Maximum Value: ");
  Serial.println(ldrCalibration.maximumValue);
}

void saveLDRCalibration() {
  EEPROM.put(ADDRESS_LDR_CALIBRATION_DATA, ldrCalibration);
  Serial.println("[LDR] Calibration saved to EEPROM");
  
  // Read back the saved data for verification
  LDRCalibrationData savedData;
  EEPROM.get(ADDRESS_LDR_CALIBRATION_DATA, savedData);
  
  Serial.print("[LDR] Minimum Value on EEPROM: ");
  Serial.println(savedData.minimumValue);
  Serial.print("[LDR] Maximum Value on EEPROM: ");
  Serial.println(savedData.maximumValue);
  
  // Compare saved data with ldrCalibration in memory
  if (savedData.minimumValue == ldrCalibration.minimumValue && savedData.maximumValue == ldrCalibration.maximumValue) {
    Serial.println("[LDR] Saved calibration data matches with memory");
  } else {
    Serial.println("[LDR] Saved calibration data does not match with memory");
  }
}



int readLuminosity()
{
  int ldrValue = analogRead(LUMINOSITY_PIN);

  // Update the minimum and maximum sensor values if necessary
  if (ldrValue < ldrCalibration.minimumValue || ldrCalibration.minimumValue == -1) {
    ldrCalibration.minimumValue = ldrValue;
    saveLDRCalibration(); // Store the updated minimum value in EEPROM
  } else if (ldrValue > ldrCalibration.maximumValue || ldrCalibration.maximumValue == -1) {
    ldrCalibration.maximumValue = ldrValue;
    saveLDRCalibration(); // Store the updated maximum value in EEPROM
  }

  // Map the sensor value to a 0-100% scale using the dynamically regulated minimum and maximum values
  // Determine mapping based on REVERSE_LDR
  if (REVERSE_LDR) {
    mappedValue = map(ldrValue, ldrCalibration.maximumValue, ldrCalibration.minimumValue, 0, 100);
  } else {
    mappedValue = map(ldrValue, ldrCalibration.minimumValue, ldrCalibration.maximumValue, 0, 100);
  }

  return mappedValue;
} 
