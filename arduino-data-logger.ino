
#include <Wire.h>

#include "RTClib.h"
#include "configuration.h"
#include "globals.h"

#include "EEPROM.h"

// clock variables
int second, minute, hour, a, d, dg, cnt, dt, month;
char months[13] = {' ', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'o', 'n', 'd'};
int l[13] = {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
int addressIndex = 0;

// states
int state = 0;
bool emitAlert = false;

// real time clock
RTC_DS3231 rtc;

// Start point
void setup()
{
  for (int i = 0; i < EEPROM.length(); i++) {
    EEPROM.write(i, 0); // Write 0 to each EEPROM address
  }
  
  Serial.begin(9600);

  initDHT();

  if (!rtc.begin()) {
    Serial.println("DS3231 not found!");
    while (1);
  }

  if (rtc.lostPower()) {
    Serial.println("DS3231 OK!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__))); //compile time date time
  }

  initScreen();

  pinMode(LED_PIN, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);

  pinMode(NAVIGATION_PIN, INPUT);
  pinMode(CONFIRM, INPUT);
  pinMode(LUMINOSITY_PIN, INPUT);
  pinMode(DHT_PIN, INPUT);
  pinMode(POTENTIOMETER, INPUT);

  setupLuminosity();

  displayScreen(); 
}

// Variables that work with milis() for time-dependant code
unsigned long screenChangedTimestamp = millis();
unsigned long blinkChangedTimestamp = millis();
unsigned long limitCalculationTimestamp = millis();


// Push button variables
unsigned long lastDebounceTime = 0;
unsigned long lastButtonPressTime = 0;
const unsigned long buttonDelay = BUTTON_CLICK_INTERVAL_MS;
const unsigned long debounceDelay = 50;
int navigationButtonState = 0;
int lastNavigationButtonState = 0;

// Sensor reading time variables
unsigned long lastSensorReadTime = 0;
const unsigned long sensorReadDelay = 200;

unsigned long lastDHTReadTime = 0;
const unsigned long dhtReadDelay = 2000;

void loop()
{
  /*
    Button logic
  */
  navigationButtonState = digitalRead(NAVIGATION_PIN);

  // Check if the button state has changed and debounce it
  if (navigationButtonState != lastNavigationButtonState) {
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    if (navigationButtonState == HIGH && (millis() - lastButtonPressTime) > buttonDelay) {
      changeScreen();
      screenChangedTimestamp = millis();
      lastButtonPressTime = millis();
    }
  }

  lastNavigationButtonState = navigationButtonState;

  /*
    Sensor readings
  */

  if (millis() - lastSensorReadTime > sensorReadDelay) {
    lastSensorReadTime = millis();

    int luminosity = 0;

    // Reading sensors
    luminosity = readLuminosity();

    // Storing the data
    addLuminosityData(luminosity);
  }

  if (millis() - lastDHTReadTime > dhtReadDelay) {
    lastDHTReadTime = millis();

    int temperature, humidity = 0;

    // Reading sensors
    temperature = readTemperature();
    humidity = readHumidity();

    // Storing the data
    addTemperatureData(temperature);
    addHumidityData(humidity);
  }

  /*
    Alert logic
  */
  
  emitAlert = outOfBounds(
    sensorData.temperatureAverage,
    sensorData.luminosityAverage,
    sensorData.humidityAverage
  );
  if (emitAlert == true) {
    saveIfNecessary();
    startAlert();
  } else {
    stopAlert();
  }

  

  /*
    Show data on screen
  */
  displayScreen();
  
  
}
