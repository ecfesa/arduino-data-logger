#include <LiquidCrystal_I2C.h>

// screen definitions
LiquidCrystal_I2C lcd(0x27, 16, 2);

enum Screen
{
  SCREEN_1,
  SCREEN_2,
  SCREEN_3
};

Screen currentScreen = SCREEN_2;

// State of blinking ':' on clock
bool isBlinkOn = true;

void initScreen() {
  lcd.init();
  lcd.backlight();

  thumbsup();
  delay(1000);

  lcd.createChar(1, bar1);
  lcd.createChar(2, bar2);
  lcd.createChar(3, bar3);
  lcd.createChar(4, bar4);
  lcd.createChar(5, bar5);
  lcd.createChar(6, bar6);
  lcd.createChar(7, bar7);
  lcd.createChar(8, bar8);

  lcd.home();
}

void displayScreen()
{
  switch (currentScreen)
  {
    case SCREEN_1:
      lcd.backlight();
      drawClock();
      break;
    case SCREEN_2:
      lcd.backlight();
      drawMeasures();
      break;
    case SCREEN_3:
      lcd.noBacklight();
      break;
  }
}

void changeScreen()
{
  currentScreen = static_cast<Screen>((currentScreen + 1) % 3);
  lcd.clear();
  displayScreen();
}


// Variables for drawClock
unsigned long lastBlinkTime = 0;
const int blinkDelay = 500;

void drawClock()
{
  DateTime now = rtc.now();

  second = now.second();
  hour = now.hour();
  minute = now.minute();
  dt = now.day();

  d = (hour) % 10;
  printNumber(d, 3);

  d = (hour) / 10;
  printNumber(d, 0);

  d = minute % 10;
  printNumber(d, 10);
  d = minute / 10;
  printNumber(d, 7);

  lcd.setCursor(14, 0);

  lcd.print(second / 10);
  lcd.print(second % 10);

  lcd.setCursor(13, 1);
  lcd.print(months[month]);
  lcd.print(dt / 10);

  lcd.print(dt % 10);

  if (millis() - lastBlinkTime > blinkDelay) {
    isBlinkOn = !isBlinkOn;
    lastBlinkTime = millis();
    if (isBlinkOn) {
      lcd.setCursor(6, 0);
      lcd.print(" ");
      lcd.setCursor(6, 1);
      lcd.print(" ");
      lcd.setCursor(13, 0);
      lcd.print(" ");
    }
    else {
      lcd.setCursor(6, 0);
      lcd.print(".");
      lcd.setCursor(6, 1);
      lcd.print(".");
      lcd.setCursor(13, 0);
      lcd.print(":");
    }
  }
}

void drawMeasures()
{
  lcd.setCursor(0, 0);
  lcd.print("Temp.:  ");
  lcd.print(sensorData.temperatureAverage, 1);
  lcd.print("C");

  lcd.setCursor(0, 1);
  lcd.print("Lum:");
  if (sensorData.luminosityAverage < 9.5) {
    lcd.print(" ");
  }
  lcd.print(sensorData.luminosityAverage, 0);
  lcd.print("%");

  if (sensorData.luminosityAverage < 99.5) {
    lcd.print(" ");
  }

  lcd.setCursor(8, 1);
  lcd.print("Hum:");
  if (sensorData.humidityAverage < 10) {
    lcd.print(" ");
  }
  lcd.print(sensorData.humidityAverage, 0);
  lcd.print("%");
}

void custom0(int col)
{
  lcd.setCursor(col, 0);
  lcd.write(2);
  lcd.write(8);
  lcd.write(1);
  lcd.setCursor(col, 1);
  lcd.write(2);
  lcd.write(6);
  lcd.write(1);
}

void custom1(int col)
{
  lcd.setCursor(col, 0);
  lcd.write(32);
  lcd.write(32);
  lcd.write(1);
  lcd.setCursor(col, 1);
  lcd.write(32);
  lcd.write(32);
  lcd.write(1);
}

void custom2(int col)
{
  lcd.setCursor(col, 0);
  lcd.write(5);
  lcd.write(3);
  lcd.write(1);
  lcd.setCursor(col, 1);
  lcd.write(2);
  lcd.write(6);
  lcd.write(6);
}

void custom3(int col)
{
  lcd.setCursor(col, 0);
  lcd.write(5);
  lcd.write(3);
  lcd.write(1);
  lcd.setCursor(col, 1);
  lcd.write(7);
  lcd.write(6);
  lcd.write(1);
}

void custom4(int col)
{
  lcd.setCursor(col, 0);
  lcd.write(2);
  lcd.write(6);
  lcd.write(1);
  lcd.setCursor(col, 1);
  lcd.write(32);
  lcd.write(32);
  lcd.write(1);
}

void custom5(int col)
{
  lcd.setCursor(col, 0);
  lcd.write(2);
  lcd.write(3);
  lcd.write(4);
  lcd.setCursor(col, 1);
  lcd.write(7);
  lcd.write(6);
  lcd.write(1);
}

void custom6(int col)
{
  lcd.setCursor(col, 0);
  lcd.write(2);
  lcd.write(3);
  lcd.write(4);
  lcd.setCursor(col, 1);
  lcd.write(2);
  lcd.write(6);
  lcd.write(1);
}

void custom7(int col)
{
  lcd.setCursor(col + 0, 0);
  lcd.write(8);
  lcd.write(8);
  lcd.write(1);
  lcd.setCursor(col, 1);
  lcd.write(32);
  lcd.write(32);
  lcd.write(1);
}

void custom8(int col)
{
  lcd.setCursor(col, 0);
  lcd.write(2);
  lcd.write(3);
  lcd.write(1);
  lcd.setCursor(col, 1);
  lcd.write(2);
  lcd.write(6);
  lcd.write(1);
}

void custom9(int col)
{
  lcd.setCursor(col, 0);
  lcd.write(2);
  lcd.write(3);
  lcd.write(1);
  lcd.setCursor(col, 1);
  lcd.write(7);
  lcd.write(6);
  lcd.write(1);
}

void printNumber(int value, int col)
{
  switch (value) {
  case 0:
    custom0(col);
    break;
  case 1:
    custom1(col);
    break;
  case 2:
    custom2(col);
    break;
  case 3:
    custom3(col);
    break;
  case 4:
    custom4(col);
    break;
  case 5:
    custom5(col);
    break;
  case 6:
    custom6(col);
    break;
  case 7:
    custom7(col);
    break;
  case 8:
    custom8(col);
    break;
  case 9:
    custom9(col);
    break;
  default:
    break;
}
}
