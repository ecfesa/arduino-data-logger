# Arduino Data Logger

Um dispositivo de monitoramento ambiental e relógio digital, baseado em Arduino. Ele é projetado para monitorar temperatura, umidade e luminosidade em um ambiente, exibindo um alerta sonoro e visual quando as medidas ficarem fora do intervalo permitido.

![An image of the sensor screen](/assets/screen.png)

## Links de Referência

* [Documentação](/DOCS.md)
* [Manual de utilização](/MANUEL.md)
* [Diagrama elétrico](/assets/design_schem.pdf)
* [Design na protoboard](/assets/design_bb.pdf)
* [Lista de Materiais](/assets/design_bom.html)
* [Vídeo de apresentação](https://youtu.be/Q4dEtQXjYRE)

## Simulando com PICSimLab

O projeto contém um arquivo `simulation.pzw` para utilização do software de simulação [PICSimLab](https://lcgamboa.github.io/picsimlab_docs/stable/).

## Dependências

- `RTClib` by Adafruit
- `LiquidCrystal` I2C by Frank de Brabander
- `DHT sensor library` by Adafruit
- `Adafruit Unified Sensor` by Adafruit
- `Adafruit BusIO` by Adafruit

## Contribuintes

- Lucas de Melo (081220017)
- Lucas Kogima (081220043)
- Luiz Eduardo Bartolassi (081220004)
- Murilo Trevejo (081220025)
