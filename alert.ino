#include <EEPROM.h>
#include <limits.h>
#include "configuration.h"
#include "globals.h"

const int EEPROM_MAX_BYTES = EEPROM.length();

typedef struct {
  unsigned long timestamp;
  short temperature;
  short humidity; 
  short luminosity;
} SensorDataPoint;

SensorDataPoint createSensorDataPoint(unsigned long timestamp, short temperature, short humidity, short luminosity);

SensorDataPoint createSensorDataPoint(unsigned long timestamp, short temperature, short humidity, short luminosity) {
  SensorDataPoint data;
  data.timestamp = timestamp;
  data.temperature = temperature;
  data.humidity = humidity;
  data.luminosity = luminosity;
  return data;
}

short currentIndex = 0;
unsigned long lastSaveTime = 0;

void loadCurrentIndex() {
  EEPROM.get(ADDRESS_CURRENT_INDEX, currentIndex);
  if (currentIndex < 0 || currentIndex > (EEPROM_MAX_BYTES-ADDRESS_DATA_START)/sizeof(SensorDataPoint)) {
    // This means that the present data is wrong/unusable
    // Therefor, the storage is cleared
    currentIndex = 0;
    for (int i = 0; i < EEPROM_MAX_BYTES; i++) {
      EEPROM.write(i, 0);
    }
    EEPROM.put(ADDRESS_CURRENT_INDEX, currentIndex);
    EEPROM.put(ADDRESS_OVERFLOWN_BOOL, false);
  }
}

short clampFloatToShort(float value) {
  // Check if the value is within the range of a short
  if (value >= SHRT_MAX) {
    return SHRT_MAX;  // Return maximum value of short
  } else if (value <= SHRT_MIN) {
    return SHRT_MIN;  // Return minimum value of short
  } else {
    return static_cast<short>(value);  // Return float value casted to short
  }
}

int nextDataAddress() {
  return ADDRESS_DATA_START + sizeof(SensorDataPoint) * currentIndex;
}

// Function to save value + timestamp
void backupCurrentValues() {   
  SensorDataPoint data = createSensorDataPoint(
    rtc.now().unixtime(),
    clampFloatToShort(sensorData.temperatureAverage),
    clampFloatToShort(sensorData.humidityAverage),
    clampFloatToShort(sensorData.luminosityAverage)
  );

  // Write data
  EEPROM.put(nextDataAddress(), data);

  currentIndex += 1;

  if (nextDataAddress() > EEPROM_MAX_BYTES) {

    currentIndex = ADDRESS_DATA_START;
    EEPROM.put(ADDRESS_OVERFLOWN_BOOL, true);
  }

  EEPROM.put(ADDRESS_CURRENT_INDEX, currentIndex);
}

void saveIfNecessary() {
  if (millis() - lastSaveTime > EEPROM_ALERT_SAVING_INTERVAL_MS || lastSaveTime == 0) {
    lastSaveTime = millis();
    backupCurrentValues();
    #if DEBUG_MODE
    dumpIntoSerial();
    #endif
  }
}

bool outOfBounds(float temperature, float luminosity, float humidity)
{
  // temp range: 15 < t < 25 ºC
  if (temperature < 15 || temperature > 25)
  {
    return true;
  }

  // lumin range: 0 < l < 30%
  if (luminosity < 0 || luminosity > 30)
  {
    return true;
  }

  // humidity range: 30% < u < 50%
  if (humidity < 30 || humidity > 50)
  {
    return true;
  }

  return false;
}

void startAlert() {
  digitalWrite(LED_PIN, HIGH);
  tone(BUZZER_PIN, 200);
}

void stopAlert() {
  digitalWrite(LED_PIN, LOW);
  noTone(BUZZER_PIN);
}

void dumpIntoSerial() {
  short currentIndex;
  bool hasOverflown;
  
  // Read currentIndex and hasOverflown from EEPROM
  EEPROM.get(ADDRESS_CURRENT_INDEX, currentIndex);
  EEPROM.get(ADDRESS_OVERFLOWN_BOOL, hasOverflown);

  // Print currentIndex and hasOverflown
  Serial.println("------------- NEW DATA -------------");
  Serial.print("Current Index: ");
  Serial.println(currentIndex);
  Serial.print("Maximum data points: ");
  Serial.println((
    EEPROM.length() - 
    sizeof(LDRCalibrationData) -
    sizeof(short) -
    sizeof(bool)
    )/sizeof(SensorDataPoint)); 
  Serial.print("Has Overflown: ");
  Serial.println(hasOverflown ? "true" : "false");

  // Print sensor data points
  Serial.println("Sensor Data Points:");

  for (short i = 0; i < currentIndex; ++i) {
    SensorDataPoint data;
    EEPROM.get(ADDRESS_DATA_START + i * sizeof(SensorDataPoint), data);

    Serial.print("Data Point ");
    Serial.println(i);
    Serial.print("  Timestamp: ");
    Serial.println(data.timestamp);
    Serial.print("  Temperature: ");
    Serial.println(data.temperature);
    Serial.print("  Humidity: ");
    Serial.println(data.humidity);
    Serial.print("  Luminosity: ");
    Serial.println(data.luminosity);
  }
}
