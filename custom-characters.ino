#include <LiquidCrystal_I2C.h>
#include "globals.h"

// clock custom numbers
byte bar1[8] =
{
  B11100,
  B11110,
  B11110,
  B11110,
  B11110,
  B11110,
  B11110,
  B11100
};
byte bar2[] =
{
  B01111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B01111
};
byte bar3[8] =
{
  B11111,
  B11111,
  B00000,
  B00000,
  B00000,
  B00000,
  B11111,
  B11111
};
byte bar4[8] =
{
  B11110,
  B11100,
  B00000,
  B00000,
  B00000,
  B00000,
  B11000,
  B11100
};
byte bar5[8] =
{
  B01111,
  B00111,
  B00000,
  B00000,
  B00000,
  B00000,
  B00011,
  B00111
};
byte bar6[8] =
{
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B11111,
  B11111
};
byte bar7[8] =
{
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00111,
  B01111
};
byte bar8[8] =
{
  B11111,
  B11111,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000
};

void thumbsup() {
  byte thumb1[8] = {B00100,B00011,B00100,B00011,B00100,B00011,B00010,B00001};
  byte thumb2[8] = {B00000,B00000,B00000,B00000,B00000,B00000,B00000,B00011};
  byte thumb3[8] = {B00000,B00000,B00000,B00000,B00000,B00000,B00001,B11110};
  byte thumb4[8] = {B00000,B01100,B10010,B10010,B10001,B01000,B11110,B00000};
  byte thumb5[8] = {B00010,B00010,B00010,B00010,B00010,B01110,B10000,B00000};
  byte thumb6[8] = {B00000,B00000,B00000,B00000,B00000,B10000,B01000,B00110};
  lcd.createChar(0, thumb1);
  lcd.createChar(1, thumb2);
  lcd.createChar(2, thumb3);
  lcd.createChar(3, thumb4);
  lcd.createChar(4, thumb5);
  lcd.createChar(5, thumb6);
  lcd.setCursor(4,1);
  lcd.write(0);
  lcd.setCursor(4,0);
  lcd.write(1);
  lcd.setCursor(5,1);
  lcd.write(2);
  lcd.setCursor(5,0);
  lcd.write(3);
  lcd.setCursor(6,1);
  lcd.write(4);
  lcd.setCursor(6,0);
  lcd.write(5);

  lcd.setCursor(7, 0);
  lcd.print("P");
  delay(200);
  lcd.setCursor(9, 1);
  lcd.print("A");
  delay(200);
  lcd.setCursor(11, 0);
  lcd.print("D");
  delay(200);
  lcd.setCursor(13, 1);
  lcd.print("L");
  delay(1000);
}
