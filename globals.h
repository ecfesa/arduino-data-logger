#ifndef GLOBALS_H
#define GLOBALS_H

#include <LiquidCrystal_I2C.h>

typedef struct {
  short minimumValue;
  short maximumValue;
} LDRCalibrationData;

typedef struct {
  int temperature[POINTS_TEMPERATURE];
  int humidity[POINTS_HUMIDITY];
  int luminosity[POINTS_LUMINOSITY];

  int temperatureIndex;
  int humidityIndex;
  int luminosityIndex;

  float temperatureAverage;
  float humidityAverage; 
  float luminosityAverage;

  bool temperatureHasInsertedOnce;
  bool humidityHasInsertedOnce;
  bool luminosityHasInsertedOnce;
} SensorDataAverages;

// Addresses for EEPROM
#define ADDRESS_CURRENT_INDEX 0
#define ADDRESS_OVERFLOWN_BOOL (sizeof(short))
#define ADDRESS_LDR_CALIBRATION_DATA (ADDRESS_OVERFLOWN_BOOL + sizeof(bool))
#define ADDRESS_DATA_START (ADDRESS_LDR_CALIBRATION_DATA + sizeof(LDRCalibrationData))

extern SensorDataAverages sensorData;
extern LiquidCrystal_I2C lcd;
#endif
