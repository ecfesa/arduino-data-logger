

// prototype here because of a weird behaviour of the Arduino IDE.

// creates the object with initial values
SensorDataAverages createSensorDataAverages();

/*
  implementations
*/

SensorDataAverages createSensorDataAverages() {
  SensorDataAverages data;
    
  // Initialize all elements of the arrays to 0
  for (int i = 0; i < POINTS_TEMPERATURE; i++) {
    data.temperature[i] = 0;
  }
  for (int i = 0; i < POINTS_HUMIDITY; i++) {
    data.humidity[i] = 0;
  }
  for (int i = 0; i < POINTS_LUMINOSITY; i++) {
    data.luminosity[i] = 0;
  }

  // Initialize index variables to 0
  data.temperatureIndex = 0;
  data.humidityIndex = 0;
  data.luminosityIndex = 0;

  data.temperatureHasInsertedOnce = false;
  data.humidityHasInsertedOnce = false;
  data.luminosityHasInsertedOnce = false;

  return data;
}

SensorDataAverages sensorData = createSensorDataAverages();

// Function to calculate the average of an array
float calculateAverage(const int* array, int size) {
    int sum = 0;
    for (int i = 0; i < size; i++) {
        sum += array[i];
    }
    return (float)sum / size;
}

// Function to return the average temperature
float getAverageTemperature() {
    return calculateAverage(sensorData.temperature, POINTS_TEMPERATURE);
}

// Function to return the average humidity
float getAverageHumidity() {
    return calculateAverage(sensorData.humidity, POINTS_HUMIDITY);
}

// Function to return the average luminosity
float getAverageLuminosity() {
    return calculateAverage(sensorData.luminosity, POINTS_LUMINOSITY);
}

// Function to add temperature data
void addTemperatureData(int temperature) {
  if (!sensorData.temperatureHasInsertedOnce) {
    // Fill the entire arrays with the first data point
    for (int i = 0; i < POINTS_TEMPERATURE; i++) {
        sensorData.temperature[i] = temperature;  
    }

    // Set 'hasInsertedOnce' flag to true
    sensorData.temperatureHasInsertedOnce = true;
  }

  // Add temperature
  sensorData.temperature[sensorData.temperatureIndex] = temperature;
  sensorData.temperatureIndex = (sensorData.temperatureIndex + 1) % POINTS_TEMPERATURE;

  sensorData.temperatureAverage = getAverageTemperature();
}

// Function to add humidity data
void addHumidityData(int humidity) {
  if (!sensorData.humidityHasInsertedOnce) {
    // Fill the entire arrays with the first data point
    for (int i = 0; i < POINTS_HUMIDITY; i++) {
        sensorData.humidity[i] = humidity; 
    }

    // Set 'hasInsertedOnce' flag to true
    sensorData.humidityHasInsertedOnce = true;
  }

  // Add humidity
  sensorData.humidity[sensorData.humidityIndex] = humidity;
  sensorData.humidityIndex = (sensorData.humidityIndex + 1) % POINTS_HUMIDITY;

  sensorData.humidityAverage = getAverageHumidity();
}

// Function to add luminosity data
void addLuminosityData(int luminosity) {
  if (!sensorData.luminosityHasInsertedOnce) {
    // Fill the entire arrays with the first data point
    
    for (int i = 0; i < POINTS_LUMINOSITY; i++) {
        sensorData.luminosity[i] = luminosity; 
    }

    // Set 'hasInsertedOnce' flag to true
    sensorData.luminosityHasInsertedOnce = true;
  }

  // Add luminosity
  sensorData.luminosity[sensorData.luminosityIndex] = luminosity;
  sensorData.luminosityIndex = (sensorData.luminosityIndex + 1) % POINTS_LUMINOSITY;

  sensorData.luminosityAverage = getAverageLuminosity();
}

